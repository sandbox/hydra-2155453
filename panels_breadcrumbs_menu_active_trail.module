<?php

/**
 * Implementation of hook_preprocess_menu_link().
 *
 * Add active trail class if the path is in the current panel breadcrumb.
 */
function panels_breadcrumbs_menu_active_trail_preprocess_menu_link(&$variables) {
  if ($current_page = page_manager_get_current_page()) {
    $conf = $current_page['handler']->conf;

    // Check if custom breadcrumb configuration is enabled.
    if (isset($conf['panels_breadcrumbs_state']) && $conf['panels_breadcrumbs_state']
        && isset($conf['panels_breadcrumbs_menu_active_trail']) && $conf['panels_breadcrumbs_menu_active_trail']) {
      $paths = array();
      foreach (explode("\n", $conf['panels_breadcrumbs_paths']) as $path) {
        $paths[] = drupal_get_normal_path(trim($path));
      }

      if (in_array($variables['element']['#href'], $paths)) {
        $variables['element']['#localized_options']['attributes']['class'][] = 'active';
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add a checkbox to turn on/off active trails by breadcrumbs.
 */
function panels_breadcrumbs_menu_active_trail_form_panels_breadcrumbs_variant_breadcrumb_form_alter(&$form, &$form_state, $form_id) {
  $handler = $form_state['handler'];
  $conf = $handler->conf;
  $conf['panels_breadcrumbs_menu_active_trail'];

  $form['settings']['panels_breadcrumbs_menu_active_trail'] = array(
    '#type' => 'checkbox',
    '#title' => 'Activate panels breadcrumbs menu active trail.',
    '#default_value' => isset($conf['panels_breadcrumbs_menu_active_trail']) ? $conf['panels_breadcrumbs_menu_active_trail'] : 1,
  );

  $submit = $form['#submit'];
  $form['#submit'] = array_merge(array('panels_breadcrumbs_menu_active_trail_submit'), $submit);
}

/**
 * Implementation of panels_breadcrumbs_menu_active_trail_submit().
 *
 * Callback function to refactor the $form_state variable so that the active
 * trail switch setting is saved.
 */
function panels_breadcrumbs_menu_active_trail_submit($form, &$form_state) {
  $form_state['handler']->conf['panels_breadcrumbs_menu_active_trail'] = $form_state['values']['panels_breadcrumbs_menu_active_trail'];
}
